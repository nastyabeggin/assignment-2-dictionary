ASM=nasm
ASMFLAGS=-f elf64
LD=ld
RM=rm
RMFLAGS=-f
FILES=lib.o dict.o main.o
INCFILES=words.inc colon.inc
PHONY: clean

lab2: $(FILES)
	$(LD) -o $@ $^
	./$@

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

%.asm: %.inc
	touch $@

main.asm: $(INCFILES)
	touch $@

clean:
	$(RM) $(RMFLAGS) *.o

