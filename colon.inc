;colon.inc
%define first 0                     ; initial element

%macro colon 2                      ; macros from 2 elements
	%%second: dq first              ; next element pointer
	db %1,0                         ; filling in the key
	%ifid %2                        ; check if second is id
	%2:
	%else
		%error "Second element is not id"
	%endif
	%define first %%second          ; change first to the next element
%endmacro