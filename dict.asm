;dict.asm
%include "lib.inc"
%define SHIFT 8

section .text

; Takes two arguments:
; pointer to a null-terminated string, 
; ponter to a dictionary beginning.
; Searches value in the whole dictionary.
; If it is found – returns address of its entry in the dictionary
; If not – 0 in rax

global find_word

find_word:
    cmp rsi, 0                  ; check if its end
    je .not_found
    add rsi, SHIFT              ; shifting value register 
    push rdi                    
    push rsi 
    call string_equals          ; compare strings, o in rax if equal 
    pop rsi 
    pop rdi 
    test rax, rax               ; logical and (1 if success)
    jnz .found                  ; then jump .found
    sub rsi, SHIFT              ; else move to the next value
    mov rsi, [rsi]
    jmp find_word               ; repeat

    .found:
        mov rax, rsi 
        ret
    
    .not_found:
        mov rax, 0
        ret
