;main.asm
%include "colon.inc"
%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define BUFFER_SIZE 256



section .rodata
enter_message: db "Enter key: ", 0
error_message: db "Key not found!", 0
buffer_limit_message: db "Reached buffer limit!", 0
success_message: db "Value: ", 0

section .bss
buffer: resb BUFFER_SIZE        ; buffer for string

section .text
global _start

_start:
    mov rdi, enter_message
    call print_string           ; print "eneter key"
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_word              ; read string to the buffer
    cmp rax, 0                  ; check if read_word failed 
    je .error                   ; then go to .error
    push rdx                    ; save word length
    mov rdi, buffer             
    mov rsi, first
    call find_word              ; trying to find word in dic
    cmp rax, 0
    je .error                   ; if rax is 0, then the error occured 
    pop rdx 
    add rax, rdx                ; shift by value
    inc rax                     ; +1 (null-terminated string)
    push rax 
    je .success

    .success:
        mov rdi, success_message; print message
        call print_string
        pop rdi                 ; print value
        call print_string 
        call print_newline
        mov rdi, 0
        call exit
      
    ; standart error
    .error:
        mov rdi, error_message
        call print_error      
        mov rdi, 1
        call exit
    
    ; error – buffer limit reached
    .buffer_limit:
        mov rdi, buffer_limit_message
        call print_error
        call print_newline
        mov rdi, 1
        call exit



